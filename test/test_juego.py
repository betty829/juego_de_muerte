import unittest
from juego_muerte.juegoMuerte import Participante
from juego_muerte.juegoMuerte import Juego

class test_juego(unittest.TestCase):
    def setUp(self):
        self.asesino=Participante()
        self.ganador=Juego()
                
    def test_JuegoMuerte(self):
        self.asesino.nroPersonas=5
        self.assertEqual(self.asesino.cargarParticipante(),[0,1,2,3,4])

    def test_Ganador(self):
        self.ganador.nroSaltos=3
        self.assertEqual(self.ganador.jugar(6),[0])





